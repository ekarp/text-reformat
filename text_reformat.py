#!/usr/bin/env python3
#
# The MIT License (MIT)
#
# Copyright (c) 2017 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse, math, sys


def split_lines_by_ratio(text, char_width, line_height, ratio):
    """Given text, split it into lines such that the ratio of the
    width to height of the text is close to the given ratio."""
    # Let n be the number of characters per line, and l be the number of lines:
    #
    # (char_width * n) / (l * line_height) = ratio  -->
    # n = l * ratio * line_height / char_width
    #
    # Let m be the number of characters in the text:
    #
    # n * l = m
    #
    # Solving for n gives:
    #
    # l = m / n
    # n = m * ratio * line_height / (char_width * n)
    # n^2 = m * ratio * line_height / char_width
    n_squared = len(text) * ratio * line_height / float(char_width)
    assert n_squared >= 0
    n = round(math.sqrt(n_squared))
    if n == 0:
        n = 1

    lines = []
    for i in range(0, len(text), n):
        lines.append(text[i:i + n])
    return lines


if __name__ == '__main__':
    # Python only makes this available on Unix in the os module. For
    # cross-platform usage, define here.
    EX_USAGE = 64

    def error_exit(msg):
        print(msg, file=sys.stderr)
        sys.exit(EX_USAGE)

    PARSER = argparse.ArgumentParser(
        description='Reformats text into the given ratio.')

    PARSER.add_argument(
        '--aspect-ratio', type=float, default=1,
        help='Aspect ratio of the text; defaults to 1.')
    PARSER.add_argument(
        '--char-width', type=float, default=1, help='Width of a character.')
    PARSER.add_argument(
        '--char-height', type=float, default=1, help='Height of a character')
    args = PARSER.parse_args()
    if args.aspect_ratio <= 0:
        error_exit('Aspect ratio must be positive.', EX_USAGE)
    if args.char_width <= 0:
        error_exit('Character width must be positive.', EX_USAGE)
    if args.char_height <= 0:
        error_exit('Character height must be positive.', EX_USAGE)

    data = sys.stdin.read()
    print('\n'.join(split_lines_by_ratio(data, args.char_width,
                                         args.char_height, args.aspect_ratio)))
