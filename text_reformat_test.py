#!/usr/bin/env python
#
# The MIT License (MIT)
#
# Copyright (c) 2017 Elliott Karpilovsky
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import math, text_reformat, unittest


def gen_ratios():
    """Returns a list of ratios to test against."""
    return (0.4, 0.5625, 1, 1.2, math.sqrt(2), 1.5, 1.58577, 2)


def gen_attr():
    """Returns a list of character width/heights to test against."""
    return (0.5, 0.6, 0.7, 0.8, 0.9, 0.999, 1)


def gen_contents():
    """Returns a list of contents for the tests."""
    for i in range(50):
        yield 'a' * i
    yield 'abcdefghijklmnopqrstuvwxyz'
    yield 'abcdefghijklmnopqrstuvwxyz' * 10
    yield 'abcdefghijklmnopqrstuvwxyz' * 100
    yield 'abcdefghijklmnopqrstuvwxyz' * 1000
    yield 'abcdefghijklmnopqrstuvwxyz' * 10000


class TextReformatTest(unittest.TestCase):
    def testConversion(self):
        for content in gen_contents():
            for width in gen_attr():
                for height in gen_attr():
                    for ratio in gen_ratios():
                        self.verifyRatio(content, width, height, ratio)

    def verifyRatio(self, content, width, height, ratio):
        """Verifies that the lines are split in such a way that the ratio is
        no more than a factor of 10 off. Note that this is only true for the
        ratios we test; in general, very small or very large ratios can be
        unboundedly off."""
        lines = text_reformat.split_lines_by_ratio(content, width, height,
                                                   ratio)
        self.assertEqual(content, ''.join(lines))
        if not lines:
            return

        # All lines except the last one should be equal length.
        for i in range(len(lines) - 2):
            self.assertEqual(len(lines[i]), len(lines[i + 1]))

        # Last line should be less than or equal to all other lines.
        if len(lines) > 1:
            self.assertTrue(len(lines[-1]) <= len(lines[-2]))

        length = len(lines[0])
        num_lines = len(lines)

        ratio2 = length * width / float(num_lines * height)
        self.assertTrue(0.1 < ratio / ratio2 < 10)


if __name__ == '__main__':
    unittest.main()
